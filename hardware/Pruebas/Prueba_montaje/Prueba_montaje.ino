
#include <Console.h> //Comunicación inalambrica YUN
#include <FileIO.h> //Escribir archivos en YUN.

int timer = 200;           // The higher the number, the slower the timing.
int ledPins[] = {3, 5, 6, 9, 10};       // an array of pin numbers to which LEDs are attached
int pinCount = 5;           // the number of pins (i.e. the length of the array)


int sensorPin0 = A0;
int sensorPin1 = A1;
int sensorPin2 = A2;
int sensorPin3 = A3;
int sensorPin4 = A4;
int sensorPin5 = A5;
int sensorPin8 = A8;
int sensorPin11 = A11;
// select the input pin for the potentiometer
int sensorValue = 0;  // variable to store the value coming from the sensor

void setup() {
  Bridge.begin();
  Console.begin();

  for (int thisPin = 0; thisPin < pinCount; thisPin++) {
  pinMode(ledPins[thisPin], OUTPUT);
  }
  
  pinMode(sensorPin0, INPUT);
  pinMode(sensorPin1, INPUT);
  pinMode(sensorPin2, INPUT);
  pinMode(sensorPin3, INPUT);
  pinMode(sensorPin4, INPUT);
  pinMode(sensorPin5, INPUT);
  pinMode(sensorPin8, INPUT);
  pinMode(sensorPin11, INPUT);

 
  digitalWrite(sensorPin0, HIGH);
  digitalWrite(sensorPin1, HIGH);
  digitalWrite(sensorPin2, HIGH);
  digitalWrite(sensorPin3, HIGH);
  digitalWrite(sensorPin4, HIGH);
    
}

void loop() {
  for (int thisPin = 0; thisPin < pinCount; thisPin++) {
    // turn the pin on:
    digitalWrite(ledPins[thisPin], HIGH);
    delay(timer);
    // turn the pin off:
    digitalWrite(ledPins[thisPin], LOW);
  }

  Console.println(analogRead(sensorPin0));
  Console.println(analogRead(sensorPin1));
  Console.println(analogRead(sensorPin2));
  Console.println(analogRead(sensorPin3));
  Console.println(analogRead(sensorPin4));
  Console.println();
  Console.print("AAux: ");
  Console.println(analogRead(sensorPin5));  //AAux
  Console.print("AI2: ");
  Console.println(analogRead(sensorPin8));
  Console.print("AI1: ");
  Console.println(analogRead(sensorPin11)); //AI1
  Console.println();

  
delay(500);
}
