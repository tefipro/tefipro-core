
#include <Console.h> //Comunicación inalambrica YUN
#include <FileIO.h> //Escribir archivos en YUN.


// select the input pin for the potentiometer

int FC_Aux = A5; //Señal digital entrada.
int contador_FC_Aux_ciclo;

bool FC_Aux_Pisado;
bool FC_Aux_Anterior;

void setup() {
  Bridge.begin();
  Console.begin();
  
  //pinMode(FC_Aux, INPUT_PULLUP);

 //  digitalWrite(sensorPin0, HIGH);
  FC_Aux_Pisado = digitalRead(FC_Aux);
  FC_Aux_Anterior = FC_Aux_Pisado;
  contador_FC_Aux_ciclo = 0;
}

void loop() {

FC_Aux_Pisado = !digitalRead(FC_Aux);
if (!FC_Aux_Pisado && FC_Aux_Anterior) {
  contador_FC_Aux_ciclo++;
  Console.println (contador_FC_Aux_ciclo);
  delay(100);
}
  FC_Aux_Anterior = FC_Aux_Pisado;
//delay(100);
}
