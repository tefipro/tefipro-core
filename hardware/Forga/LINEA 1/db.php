#!/usr/bin/php-cli
<?php
//Argumentos de entrada OEE
$estado = $argv[1];
$contador_FC1_ciclo = $argv[2];
$contador_FC1_acumulado = $argv[3];
$contador_FC2_ciclo = $argv[4];
$contador_FC2_acumulado = $argv[5];
$contador_FC3_ciclo = $argv[6];
$contador_FC3_acumulado = $argv[7];
$contador_Aux_ciclo = $argv[8];
$contador_Aux_acumulado = $argv[9];
//Argumentos de entrada referentes al equipo TEFIPRO.CORE
$segundos_ON = $argv[10];
$n_iteraciones = $argv[11];
$control = $argv[12];
//Argumentos de entrada de reserva
$reserva1 = $argv[13];
$reserva2 = $argv[14];
$reserva3 = $argv[15];

//variables de la conexión
$DBServer = 'arduinomolinozuera.cyg99gbid3jk.eu-central-1.rds.amazonaws.com'; 
$DBUser   = 'awtefipro';
$DBPass   = 'mypassword';
$DBName   = 'forgadb';  
$conn = new mysqli($DBServer, $DBUser, $DBPass, $DBName);

// check connection
if ($conn->connect_error) {
  trigger_error('Database connection failed: '  . $conn->connect_error, E_USER_ERROR);
}
$sql="INSERT INTO 075_core_LINEA1 (estado, contador_FC1_ciclo, contador_FC1_acumulado, contador_FC2_ciclo, contador_FC2_acumulado, contador_FC3_ciclo, contador_FC3_acumulado, contador_Aux_ciclo, contador_Aux_acumulado, segundos_ON, n_iteraciones, control, reserva1, reserva2, reserva3) VALUES ($estado, $contador_FC1_ciclo, $contador_FC1_acumulado, $contador_FC2_ciclo, $contador_FC2_acumulado, $contador_FC3_ciclo, $contador_FC3_acumulado, $contador_Aux_ciclo, $contador_Aux_acumulado, $segundos_ON, $n_iteraciones, $control, $reserva1, $reserva2, $reserva3)";
if($conn->query($sql) === false) {
  trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $conn->error, E_USER_ERROR);
}
// COPIA TABLA VIRGEN
$sql2="INSERT INTO 075_core_LINEA1_virgen (estado, contador_FC1_ciclo, contador_FC1_acumulado, contador_FC2_ciclo, contador_FC2_acumulado, contador_FC3_ciclo, contador_FC3_acumulado, contador_Aux_ciclo, contador_Aux_acumulado, segundos_ON, n_iteraciones, control, reserva1, reserva2, reserva3) VALUES ($estado, $contador_FC1_ciclo, $contador_FC1_acumulado, $contador_FC2_ciclo, $contador_FC2_acumulado, $contador_FC3_ciclo, $contador_FC3_acumulado, $contador_Aux_ciclo, $contador_Aux_acumulado, $segundos_ON, $n_iteraciones, $control, $reserva1, $reserva2, $reserva3)";
if($conn->query($sql2) === false) {
  trigger_error('Wrong SQL2: ' . $sql2 . ' Error: ' . $conn->error, E_USER_ERROR);
}
?>
