
/*ADAPTACIÓN A ARDUINO YUN
----------------------------

◙ Introducción de reservas (Reserva1, Reserva2, Reserva3).
◙ Ajustar resolución de entrada analogica para Arduino YUN.(Las analogicas tienen 2^10 = 1024.
◙ Escritura en archivos en YUN.
◙ Cambiar PIN de entrada señal analógica.
◙ Obtener tiempo.
◙ Revisar función de alerta, porque los pines quizas sean diferentes y esten separados.
◙ Pin botonera.
◙ envio datos hacia mySQL
◙ Conversion a tiempo epoch
◙ Revisar guardado de archivo.
◙ Crear funcion (Process) en Setup para arrancar la conexion 3G.
○ En el Setup inicia quaga.




*/

//LIBRERIAS--------------------------------------------------------------------------------------
#include <Bridge.h>
#include <Console.h> //Comunicación inalambrica YUN
#include <FileIO.h> //Escribir archivos en YUN.
#include <Process.h> // Pasar programas a linux
#include <Time.h> 

//CONFIGURACIÓN--------------------------------------------------------------------------------------
time_t FM = 60; //(Frecuencia de Muestreo en s)Máximo seguro 30min
int Umbral = 20; // Umbral para activar alerta.(Amperios)
unsigned long DelayInicio = 50000; //Delay despues del Setup, para instalaciones donde hay un falso pico al iniciar porque se energiza el sistema.(también para la conexión)
char Archivo[] = "/mnt/sda1/LOG.csv";

//DEFINICIONES--------------------------------------------------------------------------------------
int PinLectura1 = A11; //Señal analógica medida.
int PinBotonera = A7; //Pin de entrada botones.
int VoltajeBotonera;
//const int chipSelect = 10;
int FeedbackPin = 13; //No lo utilizamos de momento.

//VARIABLES PARA TRABAJAR Y REGISTRAR
int Lectura1;
unsigned long Lectura1Acumulado;
unsigned long Lectura1Media;
int Lectura1Max;
int Lectura1Min;
int Reserva1;
int Reserva2;
int Control;//Por cada error se suma 1. de tal manera que con los segundos on y este conteo podemos saber qué ha fallado. Es mejorable.
    
unsigned long ni; //numero de iteraciones en el bucle para sacar media.
int Estado; //Para almacenar el motivo de la parada.
/* Estado:
 *  =0 Funcionando
 *  =1-5 Parado por motivo 1-5 (El 5 lo reservamos para "otros")
 *  =6 Parado por motivo sin imputar.
 *  =99 Despues de iniciar el equipo.
 */
String timestamp; 
 
 //------DEFINICIÓN Variables de tiempo ----------------
time_t t0; //Tiempo inicio bucle.
time_t ta; //Tiempo actual
long id = 1;                //Use this to store the id # of our reading

void setup() {
  Bridge.begin();
  Console.begin();
  FileSystem.begin();
 //while (!Console);
 // Console.println("INICIO SETUP()");

 //------Escribir ENCABEZADO en archivo ----------------
if (!FileSystem.exists(Archivo))
    {
  String Cabecera = "ID, Segundos ON, TimeStamp, n, Lectura1Media, Lectura1Max, Lectura1Min, Estado, ";
  if (SalvadoEnArchivo (Archivo, Cabecera)){
    Console.println ("Cabecera escrita");
  }
 }
 else
 {
 Console.print("Archivo existente, no necesario escribir cabecera");
 }


//------INICIO VARIABLES TIEMPO ----------------

  t0 = now(); //
  
  ni = 0;
  delay (DelayInicio);
  Process p2;    //proceso para iniciar quagga       /etc/init.d/quagga restart  
  p2.begin("/etc/init.d/quagga");      
  p2.addParameter("restart");
  p2.run();
  while (p2.available() > 0) {
char c = p2.read();
Console.print(c);
}
  Console.println("FIN SETUP()");

//------inicio PINES LED--------------------
pinMode(2, OUTPUT);
pinMode(3, OUTPUT);
pinMode(4, OUTPUT);
pinMode(5, OUTPUT);
pinMode(7, OUTPUT);


//------INICIO VARIABLES RESERVA ----------------
Reserva1 = 0;
Reserva2 = 0;
Control = 0;

//-----DEFINICION ESTADO 99 ---------------------
Estado = 99; /*Inicializar a estado 99 porque viene de un reinicio, para que no se impute a estado 6, y además aporte información.
En principio el estado 99 debe ser tratado como parado por fin de producción (Estado 1) porque por ese motivo se paró el día anterior.*/
digitalWrite(2, HIGH);
digitalWrite(7, LOW);
digitalWrite(5, LOW);
digitalWrite(4, LOW);
digitalWrite(3, LOW);
}

void loop() {
  Console.println("INICIO LOOP ()");
  //CONVERSION del Valor
  Lectura1Media = Lectura1Media * 0.5859375; // (600A / 1024 ptos)
  Lectura1Max = Lectura1Max * 0.5859375; // (600A / 1024 ptos)
  Lectura1Min = Lectura1Min * 0.5859375; // (600A / 1024 ptos)
  Lectura1 = Lectura1 * 0.5859375; // (600A / 1024 ptos)
  //Console.println(Lectura1);


   // Definición ESTADO ON
   if (Lectura1 > Umbral) 
  {
    Estado = 0;
    digitalWrite(7, LOW);
    digitalWrite(5, LOW);
    digitalWrite(4, LOW);
    digitalWrite(3, LOW);
    digitalWrite(2, LOW);
  }
  else
  {
    if (Estado == 0) 
    {
      Estado = 6; // Viene de estar funcionando y ahora esta parado (tienen que imputar motivo)
          }
  }

//Create Data string for storing in MySQL
timestamp = String(getTimeStamp());
String dataString = String(t0)+ ", "+ timestamp+ ", "+ String(ni)+ ", "+ String(Lectura1Media)+ ", "+ String(Lectura1Max)+ ", "+ String(Lectura1Min)+ ", "+ String(Estado)+ ", "+ String(Reserva1)+ ", "+ String(Reserva2)+ ", "+ String(Control);
Process p;              
p.begin("/root/db.php");      
p.addParameter(String(t0));
p.addParameter(timestamp);
p.addParameter(String(ni));
p.addParameter(String(Lectura1Media));
p.addParameter(String(Lectura1Max));
p.addParameter(String(Lectura1Min));
p.addParameter(String(Estado));
p.addParameter(String(Reserva1));
p.addParameter(String(Reserva2));
p.addParameter(String(Control));
p.run();

while (p.available() > 0) {
char c = p.read();
Console.print(c);
Control ++; //hay que revisarlo sumaria por cada caracter, no por cada error
}
Console.println(dataString);

//Create Data string for storing and store it on SD card
dataString = String(id)+ ", "+ dataString;
if (!SalvadoEnArchivo (Archivo, dataString)){
  Control ++;
}

//Increment ID number
  id++; 

 //ESPERA tiempo hasta siguiente registro.
 //REINICIO de VARIABLES BUCLE antes de entrar en él.
  ta = now();
  ni = 0; //resetear contador de bucle, antes de entrar en el.
  Lectura1Media = Lectura1Max = Lectura1Acumulado = 0; //resetear antes de entrar en el bucle.
  Lectura1Min = 1024; //resetear antes de entrar en el bucle.
 //BUCLE
  while ((ta - t0) < FM ) 
  {
   // Console.println (ta);
    Lectura1=analogRead(PinLectura1);
    Lectura1Acumulado = Lectura1Acumulado + Lectura1;
    Lectura1Media=(Lectura1Acumulado/(ni+1));
    if (Lectura1Max < Lectura1) { Lectura1Max = Lectura1 ;}
    if (Lectura1Min > Lectura1) { Lectura1Min = Lectura1 ;}
    //String dataStringBucle = String(Lectura1)+ ", "+ String(Lectura1Media)+ ", "+ String(Lectura1Max)+ ", "+ String(Lectura1Min)+ ", "+ String(ni);
    //Console.println (dataStringBucle);
    VoltajeBotonera = analogRead(PinBotonera);
    if (VoltajeBotonera > 120) {
      if (VoltajeBotonera > 280) {
        if (VoltajeBotonera > 395) {
          if (VoltajeBotonera > 670) {
            if (VoltajeBotonera > 930) {
              Estado = 5;
              digitalWrite(7, HIGH);
              digitalWrite(5, LOW);
              digitalWrite(4, LOW);
              digitalWrite(3, LOW);
              digitalWrite(2, LOW);}
            else{
              Estado = 4;
              digitalWrite(5, HIGH);
              digitalWrite(7, LOW);
              digitalWrite(4, LOW);
              digitalWrite(3, LOW);
              digitalWrite(2, LOW);}}
          else{
            Estado = 3;
            digitalWrite(4, HIGH);
            digitalWrite(7, LOW);
            digitalWrite(5, LOW);
            digitalWrite(3, LOW);
            digitalWrite(2, LOW);}}
        else{
          Estado = 2;
          digitalWrite(3, HIGH);
          digitalWrite(7, LOW);
          digitalWrite(5, LOW);
          digitalWrite(4, LOW);
          digitalWrite(2, LOW);}}
      else{
        Estado = 1;
        digitalWrite(2, HIGH);
        digitalWrite(7, LOW);
        digitalWrite(5, LOW);
        digitalWrite(4, LOW);
        digitalWrite(3, LOW);}}
    if (Estado == 6) {
      alerta(200,2,4);
    }
    ni++;
    delay(500); //Prueba para ver si así se uelga menos (podría ser que el procesador se calentase y eso sea lo que lo cuelga)
    ta = now();
    }
t0 = ta;
Console.print ("Voltaje botonera: ");
Console.println (VoltajeBotonera);
}

//FUNCIONES -------------------------------------------------------------------------
//Función ALERTA (Hace parpadear los leds de panel)
void alerta(int refresco, int pin_ini, int n_leds) { //refresco recomendado = 200
for (int i=pin_ini; i<=(pin_ini+n_leds-1); i++){
  pinMode(i, OUTPUT); // 20160329-Para que los LED brillen más
  digitalWrite(i, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(refresco);              // wait for a second
  digitalWrite(i, LOW);    // turn the LED off by making the voltage LOW
  //delay(refresco);              // wait for a second
}
}


//función SalvadoEnArchivo
//Función que guarda una línea de datos en un archivo.
bool SalvadoEnArchivo (char Destino [], String Linea){
  bool saved;
File dataFile = FileSystem.open(Destino, FILE_APPEND);
  if (dataFile)
  {
    dataFile.println(Linea);
    dataFile.close();
    Console.print(Linea);
    Console.println(" ESCRITO EN ARCHIVO ");
    delay (500); 
    digitalWrite(FeedbackPin, LOW);
    saved = true;
  }
  else
  {
    Console.print(" ¡¡¡¡¡¡ ATENCIÓN !!!!! - ");
    Console.print(Linea);
    Console.println(" ¡¡¡¡¡¡ ATENCIÓN !!!!! - NO escrito en archivo.");
    digitalWrite(FeedbackPin, HIGH);
    saved = false;
  }  
  return saved;
}

//Función GETTIMESTAMP
String getTimeStamp() {
  Console.println("INICIO función GETTIMESTAMP");
  String result;
  Process time;
  // date is a command line utility to get the date and the time
  // in different formats depending on the additional parameter
  time.begin("date");
  time.addParameter("+%s");  // Segundos transcurridos desde 01/Ene/1970 00:00:00 (fecha epoch)
  //             T for the time hh:mm:ss
  time.run();  // run the command

  // read the output of the command
  while (time.available() > 0) {
    char c = time.read();
    if (c != '\n') {
      result += c;
    }
  }
Console.println("FIN función GETTIMESTAMP");
  return result;
}
//INFORMACIÓN DIVERSA
/*
El Integrador Ragowski da 5V a 600A. Puesto que hay 1024 ptos de entrada analgica: 600 / 1024 = 0.5859375
*/

