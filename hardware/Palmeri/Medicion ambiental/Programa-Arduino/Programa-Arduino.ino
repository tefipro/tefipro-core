/*
 TAREAS:
 ---------------------
 ○ Envio de de archivo por correo cuando tenga conexión.
 */

#include "DHT.h"
#include <Bridge.h>
#include <Console.h> //Comunicación inalambrica YUN
#include <FileIO.h> //Escribir archivos en YUN.
#include <Process.h> // Pasar programas a linux
#include <Time.h>

//CONFIGURACIÓN--------------------------------------------------------------------------------------
time_t FM = 900; //(Frecuencia de Muestreo en s)Máximo seguro 30min

char Archivo[] = "/mnt/sda1/LOG.csv"; //ARCHIVO copia de seguridad en tarjeta

//DEFINICIONES--------------------------------------------------------------------------
#define DHTPIN 7     // what pin we're connected to
#define DHTPIN2 12     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
#define POWERPIN 8 // POWER PIN para DHT1
#define POWERPIN2 13 // POWER PIN para DHT2
//int FeedbackPin = 13; //No lo utilizamos de momento porque coincide con el 13

//VARIABLES PARA TRABAJAR Y REGISTRAR
long id = 1; 
float T1;
int H1;
int P1 = 0;
float T2 = 0;
int H2 = 0;
int P2 = 0;
int Reserva1;
int Reserva2;
int Reserva3;
float Reserva4;

DHT dht(DHTPIN, DHTTYPE);
DHT dht2(DHTPIN2, DHTTYPE);

 //------DEFINICIÓN Variables de tiempo ----------------
time_t t0; //Tiempo inicio bucle.
time_t ta; //Tiempo actual

void setup() {
  pinMode(POWERPIN, OUTPUT);
  digitalWrite(POWERPIN, HIGH);
  pinMode(POWERPIN2, OUTPUT);
  digitalWrite(POWERPIN2, HIGH);
  Bridge.begin();
  Console.begin();
  dht.begin();
  dht2.begin();

 //------Escribir ENCABEZADO en archivo ----------------
if (!FileSystem.exists(Archivo))
    {
  String Cabecera = "ID, TimeStamp, T1, H1, P1, T2, H2, P2, Reserva1, Reserva2, Reserva3, Reserva4";
  SalvarEnArchivo (Archivo, Cabecera);
 }
 else
 {
 Console.print("Archivo existente, no necesario escribir cabecera");
 }


//------INICIO VARIABLES TIEMPO ----------------
t0 = now();


//------INICIO VARIABLES RESERVA ----------------
Reserva1 = 0;
Reserva2 = 0;
Reserva3 = 0;
Reserva4 = 0;

}

void loop() {
  //Console.println("INICIO LOOP ()");
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  int H1 = dht.readHumidity();
  int H2 = dht2.readHumidity();
  // Read temperature as Celsius
  float T1 = dht.readTemperature();
  float T2 = dht2.readTemperature();
  Console.print("SENSOR 1: ");
  Console.print(T1);
  Console.print(" *C "); 
  Console.print(H1);
  Console.print(" %\t ");
  Console.print("// SENSOR 2: "); 
  Console.print(T2);
  Console.print(" *C ");
  Console.print(H2);
  Console.println(" %\t ");

  //Process for storing in MySQL
Process p;              
p.begin("/root/db.php");
p.addParameter(String(T1));
p.addParameter(String(H1));
p.addParameter(String(P1));
p.addParameter(String(T2));
p.addParameter(String(H2));
p.addParameter(String(P2));
p.addParameter(String(Reserva1));
p.addParameter(String(Reserva2));
p.addParameter(String(Reserva3));
p.addParameter(String(Reserva4));
p.run();

while (p.available() > 0) {
char c = p.read();
Console.print(c);
}

//GUARDADO EN ARCHIVO
String dataString;
dataString += String(id);
dataString += ",";
dataString += String(getTimeStamp());;
dataString += ",";
dataString += String(T1);
dataString += ",";
dataString += String(H1);
dataString += ",";
dataString += String(P1);
dataString += ",";
dataString += String(T2);
dataString += ",";
dataString += String(H2);
dataString += ",";
dataString += String(P2);
dataString += ",";
dataString += String(Reserva1);
dataString += ",";
dataString += String(Reserva2);
dataString += ",";
dataString += String(Reserva3);
dataString += ",";
dataString += String(Reserva4);

SalvarEnArchivo (Archivo, dataString);
id++;


ta = now();
digitalWrite(POWERPIN, LOW);
digitalWrite(POWERPIN2, LOW);
while ((ta - t0) < FM ) 
  {
delay(500);
ta = now();
//Console.print(ta);
//Console.print(t0);
//Console.print(FM);
}
digitalWrite(POWERPIN, HIGH);
digitalWrite(POWERPIN2, HIGH);
//delay(500); //para que se energicen los sensores
t0 = ta;
}



//FUNCIONES -------------------------------------------------------------------------


//función SALVAReNaRCHIVO
//Función que guarda una línea de datos en un archivo.
void SalvarEnArchivo (char Destino [], String Linea){
File dataFile = FileSystem.open(Destino, FILE_APPEND);
  if (dataFile)
  {
    dataFile.println(Linea);
    dataFile.close();
    Console.print(Linea);
    Console.println(" ESCRITO EN ARCHIVO ");
    delay (500); 
    //digitalWrite(FeedbackPin, LOW);
  }
  else
  {
    Console.println(" ¡¡¡¡¡¡ ATENCIÓN !!!!! - NO escrito en archivo.");
    //digitalWrite(FeedbackPin, HIGH);
  }  
}

//Función GETTIMESTAMP
String getTimeStamp() {
  Console.print("INICIO función GETTIMESTAMP - ...");
  String result;
  Process time;
  // date is a command line utility to get the date and the time
  // in different formats depending on the additional parameter
  time.begin("date");
  time.addParameter("+%s");  // Segundos transcurridos desde 01/Ene/1970 00:00:00 (fecha epoch)
  //             T for the time hh:mm:ss
  time.run();  // run the command

  // read the output of the command
  while (time.available() > 0) {
    char c = time.read();
    if (c != '\n') {
      result += c;
    }
  }
Console.println("FIN función GETTIMESTAMP");
  return result;
}
