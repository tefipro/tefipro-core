/*
 TAREAS:
 ---------------------
 ○ Envio de de archivo por correo cuando tenga conexión.
 */

#include <Bridge.h>
#include <Console.h> //Comunicación inalambrica YUN
#include <FileIO.h> //Escribir archivos en YUN.
#include <Process.h> // Pasar programas a linux
#include <OneWire.h> //Conexión a través de un único cable
#include <DallasTemperature.h> //Gestion de sensor DS18B20
#include <Time.h>

//CONFIGURACIÓN--------------------------------------------------------------------------------------
time_t FM = 300; //(Frecuencia de Muestreo en s) !!Ojo cuentas con medias.

char Archivo[] = "/mnt/sda1/LOG.csv"; //ARCHIVO copia de seguridad en tarjeta

//DEFINICIONES--------------------------------------------------------------------------
#define ONE_WIRE_BUS A0 /*-(Connect to Pin A0 )-*/
#define ONE_WIRE_BUS2 A2 
#define ONE_WIRE_BUS3 A4 
#define ONE_WIRE_BUS4 A5 
//int FeedbackPin = 13; //No lo utilizamos de momento porque coincide con el 13

//VARIABLES PARA TRABAJAR Y REGISTRAR
long id = 1; 
float T1;
float T2;
float T3;
float T4;
float T5;
float T6;
float T7;
float T8;
int Reserva3;
float Reserva4;


/* Set up a oneWire instance to communicate with any OneWire device*/
OneWire ourWire(ONE_WIRE_BUS);
OneWire ourWire2(ONE_WIRE_BUS2);
OneWire ourWire3(ONE_WIRE_BUS3);
OneWire ourWire4(ONE_WIRE_BUS4);

/* Tell Dallas Temperature Library to use oneWire Library */
DallasTemperature sensors(&ourWire);
DallasTemperature sensors2(&ourWire2);
DallasTemperature sensors3(&ourWire3);
DallasTemperature sensors4(&ourWire4);

 //------DEFINICIÓN Variables de tiempo ----------------
time_t t0; //Tiempo inicio bucle.
time_t ta; //Tiempo actual

void setup() {

  Bridge.begin();
  delay(1000);
  Console.begin();

/*-( Start up the DallasTemperature library )-*/
sensors.begin();
sensors2.begin();
sensors3.begin();
sensors4.begin();

 //------Escribir ENCABEZADO en archivo ----------------
if (!FileSystem.exists(Archivo))
    {
  String Cabecera = "ID, TimeStamp, T1, T2, T3, T4, T5, T6, T7, T8, Reserva3, Reserva4";
  SalvarEnArchivo (Archivo, Cabecera);
 }
 else
 {
 Console.print("Archivo existente, no necesario escribir cabecera");
 }


//------INICIO VARIABLES TIEMPO ----------------
t0 = now();


//------INICIO VARIABLES RESERVA ----------------
//Reserva1 = 0;
//Reserva2 = 0;
Reserva3 = 0;
Reserva4 = 0;

}

void loop() {
  //Console.println("INICIO LOOP ()");
  // Reading temperature
  Console.print("Requesting temperature...");
  sensors.requestTemperatures(); // Send the command to get temperatures
  sensors2.requestTemperatures(); // Send the command to get temperatures
  sensors3.requestTemperatures(); // Send the command to get temperatures
  sensors4.requestTemperatures(); // Send the command to get temperatures
  Console.println("DONE");
  
  // Read temperature as Celsius
  float T1 = sensors.getTempCByIndex(1);
  float T2 = sensors.getTempCByIndex(0);
  float T3 = sensors2.getTempCByIndex(1);
  float T4 = sensors2.getTempCByIndex(0);
  float T5 = sensors3.getTempCByIndex(0);
  float T6 = sensors3.getTempCByIndex(1);
  float T7 = sensors4.getTempCByIndex(1);
  float T8 = sensors4.getTempCByIndex(0);
  Console.print("SENSOR 1: ");
  Console.print(T1);
  Console.print(" *C "); 
  Console.print("// SENSOR 2: "); 
  Console.print(T2);
  Console.print(" *C ");
  Console.print("SENSOR 3: ");
  Console.print(T3);
  Console.print(" *C "); 
  Console.print("// SENSOR 4: "); 
  Console.print(T4);
  Console.print(" *C ");
  Console.print("SENSOR 5: ");
  Console.print(T5);
  Console.print(" *C "); 
  Console.print("// SENSOR 6: "); 
  Console.print(T6);
  Console.print(" *C ");
  Console.print("SENSOR 7: ");
  Console.print(T7);
  Console.print(" *C "); 
  Console.print("// SENSOR 8: "); 
  Console.print(T8);
  Console.print(" *C ");

  //Process for storing in MySQL
Process p;              
p.begin("/root/db.php");
p.addParameter(String(T1));
p.addParameter(String(T2));
p.addParameter(String(T3));
p.addParameter(String(T4));
p.addParameter(String(T5));
p.addParameter(String(T6));
p.addParameter(String(T7));
p.addParameter(String(T8));
p.addParameter(String(Reserva3));
p.addParameter(String(Reserva4));
p.run();

while (p.available() > 0) {
char c = p.read();
Console.print(c);
}

//GUARDADO EN ARCHIVO
String dataString;
dataString += String(id);
dataString += ",";
dataString += String(getTimeStamp());;
dataString += ",";
dataString += String(T1);
dataString += ",";
dataString += String(T2);
dataString += ",";
dataString += String(T3);
dataString += ",";
dataString += String(T4);
dataString += ",";
dataString += String(T5);
dataString += ",";
dataString += String(T6);
dataString += ",";
dataString += String(T7);
dataString += ",";
dataString += String(T8);
dataString += ",";
dataString += String(Reserva3);
dataString += ",";
dataString += String(Reserva4);

SalvarEnArchivo (Archivo, dataString);
id++;


ta = now();
while ((ta - t0) < FM ) 
  {
delay(500);
ta = now();
//Console.print(ta);
//Console.print(t0);
//Console.print(FM);
}
t0 = ta;
}



//FUNCIONES -------------------------------------------------------------------------


//función SALVAReNaRCHIVO
//Función que guarda una línea de datos en un archivo.
void SalvarEnArchivo (char Destino [], String Linea){
File dataFile = FileSystem.open(Destino, FILE_APPEND);
  if (dataFile)
  {
    dataFile.println(Linea);
    dataFile.close();
    Console.print(Linea);
    Console.println(" ESCRITO EN ARCHIVO ");
    delay (500); 
    //digitalWrite(FeedbackPin, LOW);
  }
  else
  {
    Console.println(" ¡¡¡¡¡¡ ATENCIÓN !!!!! - NO escrito en archivo.");
    //digitalWrite(FeedbackPin, HIGH);
  }  
}

//Función GETTIMESTAMP
String getTimeStamp() {
  Console.print("INICIO función GETTIMESTAMP - ...");
  String result;
  Process time;
  // date is a command line utility to get the date and the time
  // in different formats depending on the additional parameter
  time.begin("date");
  time.addParameter("+%s");  // Segundos transcurridos desde 01/Ene/1970 00:00:00 (fecha epoch)
  //             T for the time hh:mm:ss
  time.run();  // run the command

  // read the output of the command
  while (time.available() > 0) {
    char c = time.read();
    if (c != '\n') {
      result += c;
    }
  }
Console.println("FIN función GETTIMESTAMP");
  return result;
}
