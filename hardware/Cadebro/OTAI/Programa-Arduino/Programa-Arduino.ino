/*
PENDIENTES:
----------------
◙ Reprogramar función de alarma para que siga midiendo bien en el bucle a pesar de estar en alarma.
○ Revisar cabecera SD
○ Lectura de errores al ejecutar procesos en Asincronously.
○ LIMPIAR



*/

//LIBRERIAS--------------------------------------------------------------------------------------
#include <Bridge.h>
#include <Console.h> //Comunicación inalambrica YUN
#include <FileIO.h> //Escribir archivos en YUN.
#include <Process.h> // Pasar programas a linux
#include <Time.h> 

//CONFIGURACIÓN GENERAL--------------------------------------------------------------------------------------
time_t FM = 300; //(Frecuencia de Muestreo en s)Máximo seguro 30min
int Umbral_Paro_Maquina = 35; // Umbral para activar alerta.(0- 1024)
int Umbral_Marcha_Maquina = 51; // Umbral para activar alerta.(0- 1024)
const int retraso_doble_pulsacion = 2500;

unsigned long DelayInicio = 10000; //Delay despues del Setup, para instalaciones donde hay un falso pico al iniciar porque se energiza el sistema.(también para la conexión)
char Archivo[] = "/mnt/sda1/LOG.csv";

//----------------DEFINICION FÍSICA DE PINES---------
//                   Yun analog Inputs: A0 - A5, A6 - A11 (on digital pins 4, 6, 8, 9, 10, and 12).

int AI1 = A11; //Señal analógica medida.
//la AI2 queda anulado porque debo utilizarla para el FC3 ya que el pin 7 no funciona el INPUll-UP

int FC1 = 2; //Señal digital entrada.
int FC2 = 4; //Señal digital entrada.
int FC3 = 8; //Señal digital entrada.

int BotonVerde = A0; 
int BotonAzul = A1; 
int BotonAmarillo = A2; 
int BotonRojo = A3; 
int BotonNegro = A4; 

int LED_Verde = 10; 
int LED_Azul = 9; 
int LED_Amarillo = 6; 
int LED_Rojo = 5; 
int LED_Negro = 3;

int FeedbackPin = 13;

//----------VARIABLES PARA TRABAJAR Y REGISTRAR -------
//-----Indicador OEE------
int estado; //Para almacenar el motivo de la parada.
unsigned int contador_FC1_ciclo; //Es el total de piezas, sacos,... hechos en el ciclo de medición
unsigned long contador_FC1_acumulado; // Es el total de piezas, sacos,... hechos desde encendido de TEFIPRO.Core.
int contador_FC2_ciclo; //Piezas, sacos,... defectuosos debidos al motivo 1.
int contador_FC2_acumulado; //Piezas, sacos,... defectuosos debidos al motivo 1.
int contador_FC3_ciclo; //Piezas, sacos,... defectuosos debidos al motivo 2.
int contador_FC3_acumulado; //Piezas, sacos,... defectuosos debidos al motivo 2.
int contador_Aux_ciclo; //Piezas, sacos,... defectuosos debidos al motivo 3.
int contador_Aux_acumulado; //Piezas, sacos,... defectuosos debidos al motivo 3.

int Lectura1;
unsigned long Lectura1Acumulado;
unsigned long Lectura1Media;
int Lectura1Max;
int Lectura1Min;


bool FC1_Pisado;
bool FC1_Pisado_Anterior;
bool Saco_Nuevo;
int Tms = 18; // Hablaron que hacia una media de 200 sacos / hora, luego 1saco cada 18s

bool FC2_Pisado;
bool FC2_Pisado_Anterior;

bool FC3_Pisado;
bool FC3_Pisado_Anterior;

int reserva1;
int reserva2;
int reserva3;
int control;//Por cada error se suma 1. de tal manera que con los segundos on y este conteo podemos saber qué ha fallado. Es mejorable.
    
unsigned long n_iteraciones; //numero de iteraciones en el bucle para sacar media.

Process p1;
Process p2;
bool enviado_en_p1;




/* Estado:
 *  =0 Funcionando
 *  =1-5 Parado por motivo 1-5 (El 5 lo reservamos para "otros")
 *  =6 Parado por motivo sin imputar.
 *  =99 Despues de iniciar el equipo.
 */
 
String timestamp;

 
 //------DEFINICIÓN Variables de tiempo ----------------
time_t t0; //Tiempo inicio bucle. (También indica segundos_ON)
time_t ta; //Tiempo actual

long id = 1;                //Para almacenar el nº de registro en la SD

void setup() {
 Bridge.begin();
 Console.begin();
 FileSystem.begin();



 analogReference(INTERNAL);     //Para ganar definición en las medidas analógicas
 
 pinMode(AI1, INPUT);
 //digitalWrite(AI1, HIGH);
 pinMode(FC1, INPUT_PULLUP);
 pinMode(FC2, INPUT_PULLUP);
 pinMode(FC3, INPUT_PULLUP);

 
 //while (!Console);
 //Console.println("INICIO SETUP()");

 //------Escribir ENCABEZADO en archivo ----------------
 if (!FileSystem.exists(Archivo))    {
  String Cabecera = "ID, TimeStamp, Estado, contador_FC1_ciclo, contador_FC2_ciclo, defectuosos2, defectuosos3, Segundos_ON, n_iteraciones, control, Lectura1Media, contador_FC1_acumulado, reserva3";
  if (SalvadoEnArchivo (Archivo, Cabecera)){
   Console.println ("Cabecera escrita");
  }
 }
 else {
  Console.print("Archivo existente, no necesario escribir cabecera");
 }

 //------INICIO VARIABLES ENVIO SQL ----------------
 enviado_en_p1 = false;
 
 //------CONFIGURACIÓN PULL UP BOTONES ----------------
 digitalWrite(BotonVerde, HIGH);
 digitalWrite(BotonAzul, HIGH);
 digitalWrite(BotonAmarillo, HIGH);
 digitalWrite(BotonRojo, HIGH);
 digitalWrite(BotonNegro, HIGH);
 
 //------inicio PINES LED--------------------
 pinMode(LED_Verde, OUTPUT);
 pinMode(LED_Azul, OUTPUT);
 pinMode(LED_Amarillo, OUTPUT);
 pinMode(LED_Rojo, OUTPUT);
 pinMode(LED_Negro, OUTPUT);
 
 //------INICIO VARIABLES DE TRABAJO ----------------
 
 
 FC1_Pisado = !digitalRead(FC1);
 FC1_Pisado_Anterior = FC1_Pisado;
 
 contador_FC1_ciclo = 0;
 contador_FC1_acumulado = 0;
 contador_FC2_ciclo = 0;
 contador_FC2_acumulado = 0;
 contador_FC3_ciclo = 0;
 contador_FC3_acumulado = 0;
 contador_Aux_ciclo = 0;
 contador_Aux_acumulado = 0;

  
 //------INICIO VARIABLES TIEMPO Y BUCLE----------------

 t0 = now(); //
 n_iteraciones = 0;

 //------INICIO VARIABLES RESERVA ----------------
 reserva1 = 0;
 reserva2 = 0;
 reserva3 = 0;
 control = 0;

 //-----INICIO A ESTADO 99 ---------------------
 estado = 99; /*Inicializar a estado 99 porque viene de un reinicio, para que no se impute a estado 6, y además aporte información.
 En principio el estado 99 debe ser tratado como parado por fin de producción (Estado 1) porque por ese motivo se paró el día anterior.*/
 digitalWrite(LED_Verde, HIGH);
 digitalWrite(LED_Azul, LOW);
 digitalWrite(LED_Amarillo, LOW);
 digitalWrite(LED_Rojo, LOW);
 digitalWrite(LED_Negro, LOW);

 //------DELAY DE INICIO (Evitar Picos)----------------
 delay (DelayInicio);

 Console.println("FIN SETUP()");
}

void loop() {
  //Console.println("INICIO LOOP ()");
  
  //---------------PREPARACIÓN DE VARIABLES A SALVAR-----------------
  
  Lectura1Media = Lectura1Media; // (200A / (1024*4.5/5) ptos)(Input Current:0-200A , Output Voltage: 0 - 4.5 DC)
  //Lectura1Max = Lectura1Max * 0.21701; //
  //Lectura1Min = Lectura1Min * 0.21701; // 
  Lectura1 = Lectura1; // 

  contador_FC1_acumulado = contador_FC1_acumulado + contador_FC1_ciclo;
  contador_FC2_acumulado = contador_FC2_acumulado + contador_FC2_ciclo;
  contador_FC3_acumulado = contador_FC3_acumulado + contador_FC3_ciclo;
  //contador_Aux_acumulado = contador_Aux_acumulado + contador_Aux_ciclo;

   // Definición ESTADO ON
   if (Lectura1 >= Umbral_Marcha_Maquina) 
  {
    estado = 0;
    digitalWrite(LED_Verde, LOW);
    digitalWrite(LED_Azul, LOW);
    digitalWrite(LED_Amarillo, LOW);
    digitalWrite(LED_Rojo, LOW);
    digitalWrite(LED_Negro, LOW);
  }
  else
  {
    if (estado == 0) 
    {
      estado = 6; // Viene de estar funcionando y ahora esta parado (tienen que imputar motivo)
          }
  }

//----------CREACIÓN DATASTRING--------------------------------
timestamp = String(getTimeStamp());
String dataString = timestamp+ ", "+ String(estado)+ ", "+ String(contador_FC1_ciclo)+ ", "+ String(contador_FC1_acumulado)+ ", "+ String(contador_FC2_ciclo)+ ", "+ String(contador_FC2_acumulado)+ ", "+ String(contador_FC3_ciclo)+ ", "+ String(contador_FC3_acumulado)+ ", "+ String(contador_Aux_ciclo)+ ", "+ String(contador_Aux_acumulado)+ ", "+ String(t0)+ ", "+ String(n_iteraciones)+ ", "+ String(control)+ ", "+ String(Lectura1Media)+  ", "+ String(reserva2)+ ", "+ String(reserva3);


//---------- SALVADO EN MYSQL -----------------------------
/*
 * Se crean 2 procesos de envio a sql para que si se quiere enviar muy seguido no mate el anterior van alternandose el envío de uno y otro.
 */
//Process p1;
//Process p2;
if (!enviado_en_p1) {
    p1.begin("/root/db.php");
    p1.addParameter(String(estado));      
    p1.addParameter(String(contador_FC1_ciclo));
    p1.addParameter(String(contador_FC1_acumulado));
    p1.addParameter(String(contador_FC2_ciclo));
    p1.addParameter(String(contador_FC2_acumulado));
    p1.addParameter(String(contador_FC3_ciclo));
    p1.addParameter(String(contador_FC3_acumulado));
    p1.addParameter(String(contador_Aux_ciclo));
    p1.addParameter(String(contador_Aux_acumulado));
    p1.addParameter(String(t0)); //segundos_ON
    p1.addParameter(String(n_iteraciones));
    p1.addParameter(String(control));
    p1.addParameter(String(Lectura1Media));//Primera reserva para guardar el consumo medio en ciclo.
    p1.addParameter(String(reserva2));
    p1.addParameter(String(reserva3));
    p1.runAsynchronously(); //Ojo al correr en asincronously "you can not run 2 processes the same time with the same Process instance." (Si se ve un problema, quizas podríamos hacer una segunda instacia y que cada una se ejecute en función de porque hemos salido del bucle de medida.
    enviado_en_p1 = true;
    }
  else {   
    p2.begin("/root/db.php");
    p2.addParameter(String(estado));      
    p2.addParameter(String(contador_FC1_ciclo));
    p2.addParameter(String(contador_FC1_acumulado));
    p2.addParameter(String(contador_FC2_ciclo));
    p2.addParameter(String(contador_FC2_acumulado));
    p2.addParameter(String(contador_FC3_ciclo));
    p2.addParameter(String(contador_FC3_acumulado));
    p2.addParameter(String(contador_Aux_ciclo));
    p2.addParameter(String(contador_Aux_acumulado));
    p2.addParameter(String(t0)); //segundos_ON
    p2.addParameter(String(n_iteraciones));
    p2.addParameter(String(control));
    p2.addParameter(String(Lectura1Media));//Primera reserva para guardar el consumo medio en ciclo.
    p2.addParameter(String(reserva2));
    p2.addParameter(String(reserva3));
    p2.runAsynchronously(); //Ojo al correr en asincronously "you can not run 2 processes the same time with the same Process instance." (Si se ve un problema, quizas podríamos hacer una segunda instacia y que cada una se ejecute en función de porque hemos salido del bucle de medida.
    enviado_en_p1 = false;
    }




//---------- SALVADO DATASTRING EN SD -----------------------------
dataString = String(id)+ ", "+ dataString;
if (!SalvadoEnArchivo (Archivo, dataString)){
  control ++;
  }
//Increment ID number
  id++; 


 //---------------BUCLE DE MEDIDA------------------------------
 //REINICIO de VARIABLES BUCLE antes de entrar en él.
  ta = now();
  n_iteraciones = 0; //resetear contador de bucle, antes de entrar en el.
  
  Lectura1Media = Lectura1Max = Lectura1Acumulado = 0; 
  Lectura1Min = 1024; 

  contador_FC1_ciclo = 0;
  contador_FC2_ciclo = 0;
  contador_FC3_ciclo = 0;
  //contador_Aux_ciclo = 0;
  reserva3 = 0;

 //BUCLE
  while (((((ta - t0) < (FM - 0.5 * Tms) ) || !Saco_Nuevo) && ((ta - t0) < (1.5 * FM))) || (estado == 6)) //saldremos del bucle cuando: Se estime poximo a FM (estar a 0.5 veces el Tiempo Medio entre Sacos) o se supere 1.5 veces FM. Si estado = 6 se queda esperando para imputar motivo de parada.
    {
 
    Saco_Nuevo = false;  
    
    // lectura CONSUMO MÁQUINA
    Lectura1=analogRead(AI1);
    Lectura1Acumulado = Lectura1Acumulado + Lectura1;
    Lectura1Media=(Lectura1Acumulado/(n_iteraciones+1));
    //if (Lectura1Max < Lectura1) { Lectura1Max = Lectura1 ;}
    //if (Lectura1Min > Lectura1) { Lectura1Min = Lectura1 ;}

    // lectura FC's
    FC1_Pisado = !digitalRead(FC1);
    FC2_Pisado = !digitalRead(FC2);
    FC3_Pisado = !digitalRead(FC3);

    //CONTEOS si aplica
    if (!FC1_Pisado && FC1_Pisado_Anterior) {
      contador_FC1_ciclo++;
      Console.println (contador_FC1_ciclo);
      Saco_Nuevo = true;
      delay(75);  //estabiliza la medida sino produce falsos positivos
      }
      
    if (!FC2_Pisado && FC2_Pisado_Anterior) {
      contador_FC2_ciclo++;
      delay(75);  //estabiliza la medida sino produce falsos positivos
      }
            
    if (!FC3_Pisado && FC3_Pisado_Anterior) {
      contador_FC3_ciclo++;
      delay(75);  //estabiliza la medida sino produce falsos positivos
      }
      
    FC1_Pisado_Anterior = FC1_Pisado;
    FC2_Pisado_Anterior = FC2_Pisado;
    FC3_Pisado_Anterior = FC3_Pisado;
    
    delay(75); //estabiliza la medida sino produce falsos positivos cuando vuelve
    
    //lectura BOTONERA y asignación ESTADO
   
    if (analogRead(BotonVerde) < 200) {
      estado = 1;
      digitalWrite(LED_Verde, HIGH);
      digitalWrite(LED_Azul, LOW);
      digitalWrite(LED_Amarillo, LOW);
      digitalWrite(LED_Rojo, LOW);
      digitalWrite(LED_Negro, LOW);
      reserva3 = 1;
      break;  }
    if (analogRead(BotonAzul) < 200) {
      estado = 2;
      digitalWrite(LED_Verde, LOW);
      digitalWrite(LED_Azul, HIGH);
      digitalWrite(LED_Amarillo, LOW);
      digitalWrite(LED_Rojo, LOW);
      digitalWrite(LED_Negro, LOW);
      reserva3 = 2;
      break;  }
    if (analogRead(BotonAmarillo) < 200) {
      estado = 3;
      digitalWrite(LED_Verde, LOW);
      digitalWrite(LED_Azul, LOW);
      digitalWrite(LED_Amarillo, HIGH);
      digitalWrite(LED_Rojo, LOW);
      digitalWrite(LED_Negro, LOW);
      reserva3 = 3;
      break;  }
    if (analogRead(BotonRojo) < 200) {
      estado = 4;
      digitalWrite(LED_Verde, LOW);
      digitalWrite(LED_Azul, LOW);
      digitalWrite(LED_Amarillo, LOW);
      digitalWrite(LED_Rojo, HIGH);
      digitalWrite(LED_Negro, LOW);
      reserva3 = 4;
      break;  }
    if (analogRead(BotonNegro) < 200) {
      estado = 5;
      digitalWrite(LED_Verde, LOW);
      digitalWrite(LED_Azul, LOW);
      digitalWrite(LED_Amarillo, LOW);
      digitalWrite(LED_Rojo, LOW);
      digitalWrite(LED_Negro, HIGH);
      reserva3 = 5;
      delay (retraso_doble_pulsacion/2);
      if (analogRead(BotonVerde) < 200) {
          estado = 51; // 
          analogWrite(LED_Verde, 256/16); 
          reserva3 = 51;
          delay (retraso_doble_pulsacion); 
      }
      break;  }

                                                                                                                                                                        
    if (estado == 6) { //Se apagan y encienden los LEDs cada segundo.
      if ((millis()/400) % 2) {
        digitalWrite(LED_Verde, HIGH);
        digitalWrite(LED_Azul, HIGH);
        digitalWrite(LED_Amarillo, HIGH);
        digitalWrite(LED_Rojo, HIGH);
        digitalWrite(LED_Negro, HIGH);
      }
      else
      {
        digitalWrite(LED_Verde, LOW);
        digitalWrite(LED_Azul, LOW);
        digitalWrite(LED_Amarillo, LOW);
        digitalWrite(LED_Rojo, LOW);
        digitalWrite(LED_Negro, LOW);
      }
    }
 
	if ((Lectura1 < Umbral_Paro_Maquina) && (estado == 0)) // Salir del bucle si apagan máquina para escribir el momento exacto.
	{
	 estado = 6;
   Console.println (Lectura1);
   Console.println ("((Lectura1 < Umbral_Paro_Maquina) && (estado == 0))");
   reserva3 = 6;
	  break;  
	  } 
    
  if ((estado != 0) && (Lectura1 >= Umbral_Marcha_Maquina)){
    Console.println (Lectura1);
    Console.println ("(estado != 0) && (Lectura1 >= Umbral_Marcha_Maquina)");
    reserva3 = 7;
    break;} //Salir del bucle cuando arrancan para escribir el momento exacto.

    n_iteraciones++;
    ta = now();
                                                                                                                                                          
    }

t0 = ta;

}

//FUNCIONES -------------------------------------------------------------------------

// No utilizo esta funcion porque ralentiza mucho el ciclo.
 /*
 //Función ALERTA (Hace parpadear los leds de panel)
void alerta(int refresco, int pin_ini, int n_leds) { //refresco recomendado = 200
for (int i=pin_ini; i<=(pin_ini+n_leds-1); i++){
  pinMode(i, OUTPUT); // 20160329-Para que los LED brillen más
  digitalWrite(i, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(refresco);              // wait for a second
  digitalWrite(i, LOW);    // turn the LED off by making the voltage LOW
  //delay(refresco);              // wait for a second
}
}
*/

//función SalvadoEnArchivo
//Función que guarda una línea de datos en un archivo.
bool SalvadoEnArchivo (char Destino [], String Linea){
  bool saved;
File dataFile = FileSystem.open(Destino, FILE_APPEND);
  if (dataFile)
  {
    dataFile.println(Linea);
    dataFile.close();
    Console.print(Linea);
    Console.println(" ESCRITO EN ARCHIVO ");
    //delay (500); 
    digitalWrite(FeedbackPin, LOW);
    saved = true;
  }
  else
  {
    Console.print(" ¡¡¡¡¡¡ ATENCIÓN !!!!! - ");
    Console.print(Linea);
    Console.println(" ¡¡¡¡¡¡ ATENCIÓN !!!!! - NO escrito en archivo.");
    digitalWrite(FeedbackPin, HIGH);
    saved = false;
  }  
  return saved;
}

//Función GETTIMESTAMP
String getTimeStamp() {
  //Console.println("INICIO función GETTIMESTAMP");
  String result;
  Process time;
  // date is a command line utility to get the date and the time
  // in different formats depending on the additional parameter
  time.begin("date");
  time.addParameter("+%s");  // Segundos transcurridos desde 01/Ene/1970 00:00:00 (fecha epoch)
  //             T for the time hh:mm:ss
  time.run();  // run the command

  // read the output of the command
  while (time.available() > 0) {
    char c = time.read();
    if (c != '\n') {
      result += c;
    }
  }
  //Console.println("FIN función GETTIMESTAMP");
  return result;
}

//INFORMACIÓN DIVERSA
/*

*/

